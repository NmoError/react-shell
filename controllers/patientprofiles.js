const path = require('path');
const PatientProfile = require('../models/PatientProfile');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc    Get a Patient Profile
// @route   GET api/v1/patienprofiles/:id
// @access  Public
exports.getPatientProfile = asyncHandler(async (req, res, next) => {
	const patientProfile = await PatientProfile.findById(req.params.id);

	if (!patientProfile)
		next(
			new ErrorResponse(
				`Patient not found with id of ${req.params.id}`,
				404
			)
		);
	res.status(200).json({ success: true, data: patientProfile });
});

// @desc    Create a profile
// @route   POST api/v1/patienprofiles
// @access  Private
exports.createPatientProfile = asyncHandler(async (req, res, next) => {
	// Add User to req.body
	req.body.user = req.user.id;
	const patientProfile = await PatientProfile.create(req.body);

	res.status(200).json({ success: true, data: patientProfile });
});

// @desc    Edit profile
// @route   PUT /api/v1/patientprofiles/:id
// @access  Private
exports.updatePatientProfile = asyncHandler(async (req, res, next) => {
	let patientProfile = await PatientProfile.findById(req.params.id);

	if (!patientProfile) {
		return next(
			new ErrorResponse(
				`Patient Profile not found with id of ${req.params.id}`,
				401
			)
		);
	}

	if (patientProfile.user.toString() !== req.user.id) {
		return next(new ErrorResponse(`Not authorized to do edit`, 401));
	}

	patientProfile = await PatientProfile.findOneAndUpdate(
		req.params.id,
		req.body,
		{
			new: true,
			runValidators: true,
		}
	);

	res.status(200).json({ success: true, data: patientProfile });
});

// @desc        Delete a patientProfile
// @route       DELETE /api/v1/patientProfiles/:id
// @access      Public
exports.deletePatientProfile = asyncHandler(async (req, res, next) => {
	const patientProfile = await PatientProfile.findById(req.params.id);

	if (!patientProfile) {
		next(
			new ErrorResponse(
				`Patient Profile not found with id of ${req.params.id}`,
				404
			)
		);
	}

	if (patientProfile.user.toString() !== req.user.id) {
		return next(new ErrorResponse(`Not authorized to do delete`, 401));
	}

	patientProfile.remove();

	res.status(200).json({ success: true });
});

// @desc    Upload a photo to profile
// @route   api/v1/patienprofiles/:id/photo
// @access  Private
exports.patientProfilePhotoUpload = asyncHandler(async (req, res, next) => {
	const patientProfile = await PatientProfile.findById(req.params.id);

	if (!patientProfile) {
		return next(new ErrorResponse(`No profile found`, 404));
	}

	if (patientProfile.user.toString() !== req.user.id) {
		return next(new ErrorResponse(`Not authorized to do edit`, 401));
	}

	if (!req.files) {
		return next(new ErrorResponse(`Please upload a file`, 400));
	}

	const file = req.files.file;
	// Make sure image is a photo

	if (!file.mimeType.startsWith('image')) {
		return next(new ErrorResponse(`Please upload a valid file`, 400));
	}

	if (file.size > process.env.MAX_FILE_UPLOAD) {
		return next(new ErrorResponse(`Image is too heavy`, 400));
	}

	// Custom filename
	file.name = `patientPhoto_${patientProfile._id}${
		path.parse(file.name).ext
	}`;

	file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async (err) => {
		if (err) {
			console.error(err);
			return next(
				new ErrorResponse(`Error attempting to upload the image`, 400)
			);
		}

		await PatientProfile.findByIdAndUpdate(req.params.id, {
			photo: file.name,
		});

		res.status(200).json({ success: true, data: file.name });
	});
});
