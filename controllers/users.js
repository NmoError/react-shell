const User = require('../models/User');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc    Get users
// @route   GET /api/v1/users
// @access  Public
exports.getUsers = asyncHandler(async (req, res, next) => {
	const users = await User.find(req.query);

	res.status(200).json({
		success: true,
		count: users.length,
		data: users,
	});
});

// @desc    Get user
// @route   GET /api/v1/users/:id
// @access  Public
exports.getUser = asyncHandler(async (req, res, next) => {
	const user = await User.findById(req.params.id);

	if (!user) {
		return next(
			new ErrorResponse(`User not found with id of ${req.params.id}`)
		);
	}
	res.status(200).json({ success: true, data: user });
});

// @desc    Edit user
// @route   PUT /api/v1/users/:id
// @access  Private
exports.updateUser = asyncHandler(async (req, res, next) => {
	const fieldsToUpdate = {
		name: req.body.name,
		email: req.body.email,
	};

	const user = await User.findOneAndUpdate(req.user.id, fieldsToUpdate, {
		new: true,
		runValidators: true,
	});

	if (!user) {
		return next(
			new ErrorResponse(`User not found with id of ${req.params.id}`)
		);
	}
	res.status(200).json({ success: true, data: user });
});

// @desc		Update password
// @route		PUT api/v1/auth/updatepassword
// @access	Private
exports.updatePassword = asyncHandler(async (req, res, next) => {
	const user = await User.findById(req.user.id).select('+password');

	// Check current password
	if (!(await user.matchPassowrd(req.body.currentPassword))) {
		return next(new ErrorResponse('Password is incorrect'));
	}
	user.password = req.body.newPassword;
	await user.save();

	// Create token
	const token = user.getSignedJwtToken();

	res.status(200).json({ success: true, token });
});

// @desc    Delete user
// @route   DELETE /api/v1/users/:id
// @access  Private
exports.deleteUser = asyncHandler(async (req, res, next) => {
	const user = await User.findByIdAndDelete(req.params.id);

	if (!user) {
		return next(
			new ErrorResponse(`User not found with id of ${req.params.id}`)
		);
	}

	res.status(200).json({ success: true, data: {} });
});
