const express = require('express');
const {
	getUsers,
	getUser,
	updateUser,
	deleteUser,
	updatePassword,
} = require('../controllers/users');
const router = express.Router();

const { protect } = require('../middleware/auth');

router.route('/').get(getUsers);

router
	.route('/:id')
	.get(getUser)
	.put(protect, updateUser)
	.delete(protect, deleteUser);

router.route('/updatepassword', protect, updatePassword);

module.exports = router;
