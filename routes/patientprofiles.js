const express = require('express');
const {
	patientProfilePhotoUpload,
	createPatientProfile,
	updatePatientProfile,
	getPatientProfile,
	deletePatientProfile,
} = require('../controllers/patientprofiles');
const router = express.Router();

const { protect } = require('../middleware/auth');

router.route('/:id/photo').put(protect, patientProfilePhotoUpload);
router.route('/').post(protect, createPatientProfile);
router
	.route('/:id')
	.get(getPatientProfile)
	.put(protect, updatePatientProfile)
	.delete(protect, deletePatientProfile);

module.exports = router;
