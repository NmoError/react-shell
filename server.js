const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const connectDB = require('./config/db');
const errorHandler = require('./middleware/error');
const fileUpload = require('express-fileupload');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');
const xss = require('xss-clean');
const reateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');

// Load env variables
dotenv.config({ path: './config/config.env' });

// Connect to database
connectDB();

// Route Files
const auth = require('./routes/auth');
const users = require('./routes/users');
const patientProfiles = require('./routes/patientprofiles');
const app = express();

// Body Parser
app.use(express.json());

// Log middleware when developing
if (process.env.NODE_ENV === 'development') {
	app.use(morgan('dev'));
}

// File uploading (s3)
app.use(fileUpload());

// Sanitize Mongo
app.use(mongoSanitize());

// Security headers
app.use(helmet());

app.use(xss());

// Rate limiting
const limiter = reateLimit({ windowMs: 10 * 60 * 1000, max: 100 });
app.use(limiter);

// Prevent http param pollution
app.use(hpp());

// Enable cors
app.use(cors());

// Mount Express Routers
app.use('/api/v1/auth', auth);
app.use('/api/v1/users', users);
app.use('/api/v1/patientprofiles', patientProfiles);
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(
	PORT,
	console.log(
		`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`
	)
);

// Handle Promise Rejections
process.on('rejection', (err, promise) => {
	console.log(`Err: ${err.message} from server.js`);
	// Close Server and exit process
	server.close(() => process.exit(1));
});
