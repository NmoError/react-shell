import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components';

export default function Category({ title, color }) {
  return (
    <TouchableOpacity>
      <Root>
        <IconSquare color={color}>{getIcon(title)}</IconSquare>
        <Title>{title}</Title>
      </Root>
    </TouchableOpacity>
  );
}

const getIcon = (title) => {
  switch (title) {
    case 'Urgent Care':
      return <Icon source={require(`../../assets/icons/organs.png`)} alt='' />;

    case 'Dental':
      return <Icon source={require(`../../assets/icons/tooth.png`)} alt='' />;

    case 'Physio':
      return <Icon source={require(`../../assets/icons/physio.png`)} alt='' />;

    case 'Mental Health':
      return <Icon source={require(`../../assets/icons/brain.png`)} alt='' />;

    case 'Cardiology':
      return <Icon source={require(`../../assets/icons/heart.png`)} alt='' />;

    case 'Orthopedic':
      return <Icon source={require(`../../assets/icons/bone.png`)} alt='' />;

    case 'Radiology':
      return (
        <Icon source={require(`../../assets/icons/radiology.png`)} alt='' />
      );

    case 'Skin Care':
      return <Icon source={require(`../../assets/icons/skin.png`)} alt='' />;
  }
};

const Root = styled.View`
  background: white;
  height: 165px;
  width: 130px;
  border-radius: 14px;
  margin: 20px;
  box-shadow: 0 6px 8px rgba(0, 0, 0, 0.09);
  padding: 20px 0 35px 20px;
`;
const IconSquare = styled.View`
  position: absolute;
  width: 95px;
  height: 95px;
  background-color: ${(props) => props.color};
  border-radius: 14px;
  right: -10px;
  top: -10px;
`;

const Title = styled.Text`
  margin: auto auto 0 0;
  font-weight: 600;
  color: rgba(118, 118, 118, 1);
`;

const Icon = styled.Image`
  width: 50%;
  resize-mode: contain;
  margin: auto;
`;
