import * as React from 'react';
import { Dimensions, TouchableOpacity } from 'react-native';

import CircleIcon from './Tiny/CircleIcon';
import styled from 'styled-components';

export default function BodyStats({
	title,
	subtitle,
	navigation,
	destination,
}) {
	return (
		<TouchableOpacity onPress={() => navigation.push(`${destination}`)}>
			<Root>
				<LeftSide>
					<CircleIcon />
				</LeftSide>
				<RightSide>
					<Title>{title}</Title>
					{subtitle && <Subtitle>{subtitle}</Subtitle>}
				</RightSide>
			</Root>
		</TouchableOpacity>
	);
}

const ww = Dimensions.get('window').width;

const LeftSide = styled.View`
	flex: 1;
`;

const RightSide = styled.View`
	flex: 3.5;
	margin: auto;
`;

const Root = styled.View`
	flex-direction: row;
	width: ${ww * 0.8};
	margin: 8px auto;
	border-radius: 14px;
	background-color: white;
	box-shadow: 0 6px 8px rgba(0, 0, 0, 0.07);
	padding: 15px 0;
`;

const Title = styled.Text`
	font-size: 16px;
	font-weight: 600;
	margin-bottom: 5px;
	color: rgba(0, 0, 0, 0.7);
`;

const Subtitle = styled.Text`
	font-size: 11px;
	color: #535353;
`;
