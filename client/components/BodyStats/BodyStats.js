import * as React from 'react';
import { Share, Dimensions, ActionSheetIOS } from 'react-native';
import styled from 'styled-components';
import BodyImage from '../../assets/images/body.png';

import temperature from '../../assets/icons/temperature.png';
import oxygen from '../../assets/icons/oxygen.png';
import heartbeat from '../../assets/icons/heartbeat.png';

export default function BodyStats() {
	const onShare = async () => {
		try {
			const result = await Share.share({
				message: 'Share your progress with your friends',
			});
			if (result.action === Share.sharedAction) {
				if (result.activityType) {
					// shared with activity type of result.activityType
				} else {
					// shared
				}
			} else if (result.action === Share.dismissedAction) {
				// dismissed
			}
		} catch (error) {
			alert(error.message);
		}
	};

	const actionSheet = () =>
		ActionSheetIOS.showActionSheetWithOptions(
			{
				options: ['Cancel', 'Share'],
				cancelButtonIndex: 0,
			},
			(buttonIndex) => {
				if (buttonIndex === 0) {
					// cancel action
				} else if (buttonIndex === 1) {
					onShare();
				}
			}
		);

	return (
		<Root onLongPress={actionSheet}>
			<Body source={BodyImage}></Body>
			<LeftSection>
				<BodyStat>
					<Division>
						<BodyValue>5'3"</BodyValue>
						<TypeText>Height</TypeText>
					</Division>
				</BodyStat>
				<BodyStat>
					<Division>
						<BodyValue>110</BodyValue>
						<TypeText>Weight</TypeText>
					</Division>
				</BodyStat>
				<BodyStat>
					<Division>
						<BodyValue>20.4</BodyValue>
						<TypeText>Average</TypeText>
					</Division>
				</BodyStat>
			</LeftSection>
			<RightSection>
				<BodyStat>
					<Icon source={heartbeat} />
					<Division width='65'>
						<Value>102</Value>
						<TypeText>Heart Beat</TypeText>
					</Division>
				</BodyStat>
				<BodyStat>
					<Icon source={oxygen} />
					<Division width='65'>
						<Value>96</Value>
						<TypeText>Oxygen Saturation</TypeText>
					</Division>
				</BodyStat>
				<BodyStat>
					<Icon source={temperature} />
					<Division width='65'>
						<Value>80.5F</Value>
						<TypeText>Body Temperature</TypeText>
					</Division>
				</BodyStat>
			</RightSection>
		</Root>
	);
}

const ww = Dimensions.get('window').width;

const Root = styled.TouchableOpacity`
	width: ${ww - 40}px;
	height: 300px;
	margin: 0 auto 14px;
	background: white;
	border-radius: 14px;
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
	flex: 1;
	flex-direction: row;
`;
const RightSection = styled.View`
	flex: 1;
	position: absolute;
	right: 0;
	width: 40%;
	height: 100%;
	background: rgba(157, 225, 198, 0.5);
	overflow: hidden;
	border-top-right-radius: 14px;
	border-bottom-right-radius: 14px;
`;

const LeftSection = styled.View`
	margin-left: -10px;
`;

const Body = styled.Image`
	height: 70%;
	margin: auto 0;
	resize-mode: contain;
`;

const Division = styled.View`
	width: ${(props) => (props.width ? props.width + '%' : 'auto')};
`;

const BodyStat = styled.View`
	height: 33%;
	align-items: center;
	justify-content: center;
	flex-direction: row;
`;

const Value = styled.Text`
	font-size: 20px;
	color: rgba(16, 177, 150, 1);
	font-weight: 700;
`;

const BodyValue = styled.Text`
	font-size: 20px;
	color: rgba(250, 160, 114, 1);
	font-weight: 700;
`;

const TypeText = styled.Text`
	color: rgba(0, 0, 0, 0.6);
`;

const Icon = styled.Image`
	flex: 1;
	height: 27px;
	resize-mode: contain;
`;
