import * as React from 'react';
import styled from 'styled-components';
import Profile from '../assets/images/asdf.png';

export default function ProfileBrief({ navigation }) {
	return (
		<IntroSection>
			<ProfileImage source={Profile}></ProfileImage>
			<ProfileDescription>
				<Name>Hello, Alejandro</Name>
				<Subtitle>Here's your basic medical information</Subtitle>
			</ProfileDescription>
		</IntroSection>
	);
}

const IntroSection = styled.View`
	flex-direction: row;
	padding: 0 10px 24px 15px;
	margin: 15px 20px 0 15px;
`;

const ProfileDescription = styled.View``;

const ProfileImage = styled.Image`
	width: 40px;
	height: 40px;
	border-radius: 24px;
	margin-right: 14px;
`;

const Name = styled.Text`
	font-size: 24px;
	color: rgba(0, 0, 0, 0.6);
	font-weight: 500;
`;

const Subtitle = styled.Text`
	font-size: 10px;
	color: rgba(0, 0, 0, 0.5);
	font-weight: 500;
`;
