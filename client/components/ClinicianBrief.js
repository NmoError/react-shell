import * as React from 'react';
import styled from 'styled-components';
import Profile from '../assets/images/doc.jpeg';

export default function ClinicianBrief({ navigation, doctor, date }) {
  return (
    <IntroSection>
      <ClinicianImage source={Profile}></ClinicianImage>
      <ClinicianDescription>
        <Name>Recommendations from {doctor}, M.D.</Name>
        <Date>{date}</Date>
      </ClinicianDescription>
    </IntroSection>
  );
}

const IntroSection = styled.View`
  flex: 1;
  flex-direction: row;
`;

const ClinicianDescription = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
  height: 80px;
`;

const ClinicianImage = styled.Image`
  width: 80px;
  height: 80px;
  border-radius: 40px;
  margin-right: 20px;
`;

const Name = styled.Text`
  font-size: 18px;
  color: rgba(0, 0, 0, 0.6);
  font-weight: 600;
`;

const Date = styled.Text`
  font-size: 15px;
  color: rgba(0, 0, 0, 0.5);
  font-weight: 500;
`;
