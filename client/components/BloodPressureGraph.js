import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Dimensions, TouchableOpacity } from 'react-native';
import styled from 'styled-components';

export default function BloodPressureGraph({ navigation }) {
	return (
		<TouchableOpacity onPress={() => navigation.push('Medical Card')}>
			<Root>
				<Info>
					<DivisionLeft>
						<Title>Blood Pressure</Title>
						<Subtitle>Today</Subtitle>
					</DivisionLeft>
					<DivisionRight>
						<Numbers>90/60</Numbers>
						<Recommended>Recommended: 120/90</Recommended>
					</DivisionRight>
				</Info>
			</Root>
		</TouchableOpacity>
	);
}
const ww = Dimensions.get('window').width;

const Root = styled.View`
	width: ${ww - 40};
	height: 130px;
	margin: 5px 20px;
	background: rgba(257, 195, 179, 0.6);
	border-radius: 14px;
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
`;

const Info = styled.View`
	padding: 15px 0 0 25px;
	flex-direction: row;
	width: ${ww * 0.53};
`;

const DivisionLeft = styled.View`
	flex: 1;
`;

const DivisionRight = styled.View`
	flex: 2;
	margin-left: 10px;
`;

const Title = styled.Text`
	font-size: 13px;
	font-weight: 700;
	color: rgba(244, 110, 103, 1);
`;

const Subtitle = styled.Text`
	color: #fff;
	font-weight: 500;
`;

const Numbers = styled.Text`
	color: white;
	font-size: 20px;
	font-weight: 700;
	margin-bottom: 5px;
`;

const Recommended = styled.Text`
	font-size: 10px;
	padding: 1px;
	background: white;
	color: rgba(0, 0, 0, 0.4);
	border-radius: 2px;
	overflow: hidden;
	text-align: center;
`;
