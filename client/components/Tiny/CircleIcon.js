import * as React from 'react';
import styled from 'styled-components';

export default function CircleIcon({ navigation }) {
	return (
		<Icon>
			<Dot></Dot>
		</Icon>
	);
}

const Icon = styled.View`
	background: #9de1c6;
	width: 30px;
	height: 30px;
	border-radius: 20px;
	margin: auto;
`;

const Dot = styled.View`
	background: white;
	width: 14px;
	height: 14px;
	border-radius: 20px;
	margin: auto;
`;
