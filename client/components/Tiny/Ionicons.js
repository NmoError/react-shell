import { Ionicons } from '@expo/vector-icons';
import * as React from 'react';

import Colors from '../../constants/Colors';

export default function Icon(props) {
  return (
    <Ionicons
      name={props.name}
      size={props.size}
      style={{
        marginRight: props.mr,
        marginLeft: props.ml,
        marginTop: 'auto',
        marginBottom: 'auto',
      }}
      color={
        props.color
          ? props.color
          : props.active
          ? Colors.tabIconSelected
          : Colors.tabIconDefault
      }
    />
  );
}
