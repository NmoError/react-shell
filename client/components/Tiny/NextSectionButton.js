import * as React from 'react';
import styled from 'styled-components';
import Ionicons from './Ionicons';

export default function CircleIcon({ name, ioName, ioSize, mr, ml, active }) {
	if (name) {
		return (
			<Button>
				<ButtonText>{name}</ButtonText>
				<Access>
					<Ionicons
						name={ioName}
						size={ioSize}
						mr={mr}
						ml={ml}
						active={active}
					/>
				</Access>
			</Button>
		);
	}

	return (
		<Button>
			<Ionicons
				name={ioName}
				size={ioSize}
				mr={mr}
				ml={ml}
				active={active}
			/>
		</Button>
	);
}

const Button = styled.View`
	width: 100%;
	height: 60px;
	background: rgba(245, 246, 250, 1);
	border-radius: 8px;
	margin: 15px auto;
	justify-content: center;
	padding-left: 14px;
`;

const ButtonText = styled.Text`
	color: rgba(0, 0, 0, 0.6);
	font-weight: 600;
	letter-spacing: 1px;
	font-size: 16px;
`;

const Access = styled.View`
	width: 34px;
	height: 34px;
	background: white;
	position: absolute;
	right: 14px;
	border-radius: 6px;
`;
