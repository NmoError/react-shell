import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Dimensions,
} from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components';
import CircleIcon from '../Tiny/CircleIcon';

export default function MedicalTest({
	navigation,
	fragment: { fragName, fragDesc, fragScore, fragStatus },
}) {
	return (
		<Root>
			<ZoneColor />
			<InfoBody>
				<HeaderSection>
					<TypeSection>
						<TestTitle>{fragName}</TestTitle>
						<MiniTitle>{fragDesc}</MiniTitle>
					</TypeSection>
					<Result>{fragScore}</Result>
				</HeaderSection>
				<DivisionLine />
				<More>
					<MiniResult>Consult your doctor</MiniResult>
					<MiniStatus>{fragStatus}</MiniStatus>
				</More>
			</InfoBody>
		</Root>
	);
}

const Root = styled.View`
	flex-direction: row;
	background-color: rgba(243, 243, 243, 1);
	margin-bottom: 15px;
`;

const HeaderSection = styled.View`
	flex-direction: row;
	margin-bottom: 20px;
`;

const TypeSection = styled.View`
	flex-direction: column;
`;

const More = styled.View`
	flex: 1;
	flex-direction: row;
`;

const DivisionLine = styled.View`
	width: 100%;
	background: white;
	height: 2px;
	margin-bottom: 4px;
`;

const InfoBody = styled.View`
	flex: 1;
	flex-direction: column;
	padding: 15px 20px 10px 40px;
`;

const MiniTitle = styled.Text`
	font-size: 10px;
	color: rgba(118, 118, 118, 1);
`;

const MiniResult = styled.Text`
	font-size: 10px;
	color: rgba(255, 0, 0, 0.8);
`;

const TestTitle = styled.Text`
	margin: auto 0;
	font-size: 20px;
	color: rgba(118, 118, 118, 1);
	font-weight: 600;
`;

const Result = styled.Text`
	font-size: 30px;
	font-weight: 600;
	color: rgba(118, 118, 118, 1);
	margin: auto 0 auto auto;
`;

const MiniStatus = styled.Text`
	font-size: 10px;
	color: rgba(255, 0, 0, 0.8);
	margin: auto 0 auto auto;
`;

const ZoneColor = styled.View`
	width: 8px;
	height: 60%;
	margin: 10px 0;
	background: rgba(255, 0, 0, 0.6);
	border-top-right-radius: 2px;
	border-bottom-right-radius: 2px;
`;
