import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Dimensions,
} from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components';
import CircleIcon from '../Tiny/CircleIcon';

import TestFragment from './TestFragment';

export default function MedicalTest({
	navigation,
	testType,
	testDate,
	testFragments,
}) {
	return (
		<Test>
			<TitleSection>
				<IconSquare>
					<CircleIcon />
				</IconSquare>
				<TestTitle>{testType}</TestTitle>
				<Date>{testDate}</Date>
			</TitleSection>
			{testFragments.map((fragment) => (
				<TestFragment fragment={fragment} />
			))}
		</Test>
	);
}
const ww = Dimensions.get('window').width;

const Test = styled.View`
	border-radius: 20px;
	overflow: hidden;
	background: white;
	margin-bottom: 40px;
`;

const TitleSection = styled.View`
	flex-direction: row;
	background: white;
	padding: 20px;
`;

const IconSquare = styled.View``;

const TestTitle = styled.Text`
	margin: auto 10px;
	font-size: 20px;
	color: rgba(118, 118, 118, 1);
	font-weight: 600;
`;
const Date = styled.Text`
	font-size: 12px;
  color: rgba(16, 177, 150, 1)
	margin: auto 0 auto auto;
`;
