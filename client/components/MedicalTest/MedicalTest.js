import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	Dimensions,
} from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components';

import TestCluster from './TestCluster';

export default function MedicalTest({ navigation }) {
	return (
		<Root>
			<ScrollView style={{ padding: 20, paddingTop: 0 }}>
				<TestCluster
					testType='Blood Test'
					testDate='March 20, 2020'
					testFragments={dummy}
				/>
				<TestCluster
					testType='Blood Test'
					testDate='July 10, 2019'
					testFragments={dummy}
				/>
				<TestCluster
					testType='Blood Test'
					testDate='February 8, 2019'
					testFragments={dummy}
				/>
			</ScrollView>
		</Root>
	);
}

let dummy = [
	{
		fragName: 'RBC',
		fragDesc: 'Red Blood Cells',
		fragScore: 4.3,
		fragStatus: 'Attention',
	},
	{
		fragName: 'RBC',
		fragDesc: 'Red Blood Cells',
		fragScore: 4.3,
		fragStatus: 'Attention',
	},
	{
		fragName: 'RBC',
		fragDesc: 'Red Blood Cells',
		fragScore: 4.3,
		fragStatus: 'Attention',
	},
	{
		fragName: 'RBC',
		fragDesc: 'Red Blood Cells',
		fragScore: 4.3,
		fragStatus: 'Attention',
	},
];

const ww = Dimensions.get('window').width;

const Root = styled.View`
	width: ${ww};
	box-shadow: 0 6px 8px rgba(0, 0, 0, 0.07);
`;

const Date = styled.Text`
	font-size: 12px;
  color: rgba(16, 177, 150, 1)
	margin: auto 0 auto auto;
`;
