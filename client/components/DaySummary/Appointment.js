import * as React from 'react';
import styled from 'styled-components';
import { TouchableOpacity, Linking } from 'react-native';
import Profile from '../../assets/images/doc.jpeg';

import NextSectionButton from '../Tiny/NextSectionButton';

import Ionicons from '../Tiny/Ionicons';

const Appointment = ({ navigation, doctor, date, time, location }) => {
	const [call, setCall] = React.useState(false);

	const ContactButton = () => {
		return call ? (
			<Touch onPress={() => Linking.openURL(`tel:61405181591`)}>
				<NextSectionButton
					ioName='ios-call'
					ioSize={34}
					mr='auto'
					ml='auto'
				/>
			</Touch>
		) : (
			<Touch onPress={() => setCall(!call)}>
				<NextSectionButton
					name='Change Appointment'
					ioName='md-arrow-forward'
					ioSize={20}
					mr='auto'
					ml='auto'
				/>
			</Touch>
		);
	};

	return (
		<Root>
			<IntroSection>
				<ClinicianImage source={Profile}></ClinicianImage>
				<ClinicianDescription>
					<Name>{doctor}, M.D.</Name>
					<Parallel>
						<Ionicons name='md-calendar' size={20} mr={5} ml={0} />
						<Date>{date}</Date>
					</Parallel>
					<Parallel>
						<Ionicons name='md-time' size={20} mr={5} ml={0} />
						<Date>{time}</Date>
					</Parallel>
					<Parallel>
						<Ionicons name='ios-pin' size={20} mr={10} ml={2} />
						<Date>{location}</Date>
					</Parallel>
				</ClinicianDescription>
			</IntroSection>
			{ContactButton()}
			<Bell onPress={() => navigation.push('NotificationModal')}>
				<Ionicons
					name='md-notifications'
					size={30}
					mr='auto'
					ml='auto'
					active={true}
				/>
			</Bell>
		</Root>
	);
};

const Root = styled.View``;

const Bell = styled.TouchableOpacity`
	position: absolute;
	top: -10px;
	right: -5px;
`;

const IntroSection = styled.View`
	flex: 1;
	flex-direction: row;
`;

const Touch = styled.TouchableOpacity``;

const Parallel = styled.View`
	flex-direction: row;
	margin-bottom: 5px;
`;

const ClinicianDescription = styled.View`
	flex-direction: column;
	justify-content: space-between;
`;

const ClinicianImage = styled.Image`
	width: 70px;
	height: 70px;
	border-radius: 40px;
	margin-right: 20px;
`;

const Name = styled.Text`
	font-size: 18px;
	color: rgba(0, 0, 0, 0.6);
	font-weight: 600;
	margin-bottom: 10px;
`;

const Date = styled.Text`
	font-size: 15px;
	color: rgba(0, 0, 0, 0.5);
	font-weight: 500;
`;

export default Appointment;
