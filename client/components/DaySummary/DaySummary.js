import * as React from 'react';
import styled from 'styled-components';
import Appointment from './Appointment';

export default function DaySummary({
	event: { clinician, day, time, location },
	navigation,
}) {
	return (
		<Root>
			<Appointment
				navigation={navigation}
				doctor='David Brown'
				date='Thursday, March 20'
				time='15:00'
				location='Mayo Medical Clinic'
			/>
		</Root>
	);
}

const Root = styled.View`
	margin: 20px;
	border-radius: 14px;
	box-shadow: 0 4px 12px rgba(0, 0, 0, 0.07);
	background: white;
	padding: 25px 25px 5px 25px;
`;

const Title = styled.Text``;
