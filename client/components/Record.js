import * as React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styled from 'styled-components';
import { Dimensions } from 'react-native';
import ClinicianBrief from './ClinicianBrief';

export default function Record({ navigation, doctor, date }) {
	const [open, setOpen] = React.useState(false);

	return (
		<Root>
			<TouchableOpacity onPress={() => setOpen(!open)}>
				<ClinicianBrief
					doctor='David Brown'
					date='Thursday, July 5'
					open={open}
				/>
				{open ? (
					<SessionBrief>
						<InfoType>
							PatientComplaints:
							<Ans> sleep deprivation</Ans>
						</InfoType>
						<InfoType>
							Terms:
							<Ans> for 3 weeks</Ans>
						</InfoType>
						<InfoType>
							Recommendations
							<Ans> diet Restrictions (Alcohol, sugar)</Ans>
						</InfoType>
						<InfoType>
							Prescription:
							<Ans> anxiPam 20mg, once a day</Ans>
						</InfoType>
						<InfoType>
							Follow Up Appointment:
							<Ans> 12:30 PM, July 20</Ans>
						</InfoType>
					</SessionBrief>
				) : null}
			</TouchableOpacity>
		</Root>
	);
}
const ww = Dimensions.get('window').width;

const Root = styled.View`
	flex: 1;
	width: ${ww - 40}px;
	min-height: ${(props) => (props.open ? 300 : 'auto')};
	background: white;
	padding: 25px 25px 35px 25px;
	box-shadow: 0 4px 12px rgba(0, 0, 0, 0.07);
	margin: 10px auto;
	border-radius: 14px;
`;

const SessionBrief = styled.View`
	flex: 1;
	margin: 35px auto 0 auto;
`;

const InfoType = styled.Text`
	margin: 10px 0;
	font-weight: 600;
	font-size: 16px;
	color: rgba(0, 0, 0, 0.7);
`;

const Ans = styled.Text`
	margin: 5px 0;
	font-weight: 500;
	font-size: 16px;
	color: rgba(0, 0, 0, 0.5);
`;
