import * as React from 'react';
import { TouchableOpacity, View } from 'react-native';
import styled from 'styled-components';
import Ionicons from './Tiny/Ionicons';
import Profile from '../assets/images/doc.jpeg';
import ProfileWide from '../assets/images/doc_wide.jpeg';
import FakeMapp from '../assets/images/fakemap.jpg';

export default function ClinicianCard() {
	const [full, setFull] = React.useState(true);
	return (
		<TouchableOpacity onPress={() => setFull(!full)}>
			{full ? (
				<Root>
					<ClinicianPhoto source={Profile} />
					<RightSide>
						<Title>David Brown, M.D.</Title>
						<Parallel>
							<Specialty>Cardiologist</Specialty>
						</Parallel>
						<Parallel>
							<Ionicons
								name='ios-pin'
								size={20}
								mr={8}
								ml={3}
								active={true}
							/>
							<Specialty>"Mayo Medical Clinic"</Specialty>
						</Parallel>
						<Parallel>
							<Ionicons
								name='ios-star'
								size={20}
								mr={8}
								ml={0}
								active={true}
							/>
							<Specialty>4/5 (327 reviews)</Specialty>
						</Parallel>
					</RightSide>
				</Root>
			) : (
				<FullRoot>
					<Background source={ProfileWide} />
					<DescriptionView>
						<Parallel>
							<View>
								<FullTitle>David Brown, M.D.</FullTitle>
								<Parallel>
									<Specialty>Cardiologist</Specialty>
								</Parallel>
								<Parallel>
									<Ionicons
										name='ios-pin'
										size={20}
										mr={8}
										ml={3}
										active={true}
									/>
									<Specialty>"Mayo Medical Clinic"</Specialty>
								</Parallel>
								<Parallel>
									<Ionicons
										name='ios-star'
										size={20}
										mr={8}
										ml={0}
										active={true}
									/>
									<Specialty>4/5 (327 reviews)</Specialty>
								</Parallel>
							</View>
							<FakeMap source={FakeMapp} />
						</Parallel>
						<AboutTitle>About Dr. Brown</AboutTitle>
						<About>
							Dr. D. Brown, is a senior doctor - practicing as a
							Cardiologist for the last 20 years. He completed a
							PhD in Medicine in Harvard, U.S.
						</About>
					</DescriptionView>
				</FullRoot>
			)}
		</TouchableOpacity>
	);
}

const FullRoot = styled.View`
	flex: 1;
	background: white;
`;

const Background = styled.Image`
	width: 100%;
`;

const FakeMap = styled.Image`
	height: 100px;
	width: 100px;
	margin: auto 0 auto auto;
	border-radius: 50px;
`;

const DescriptionView = styled.View`
	flex: 1;
	background: white;
	top: -30px;
	height: 200px;
	border-top-right-radius: 30px;
	border-top-left-radius: 30px;
	padding: 25px 40px;
`;

const FullTitle = styled.Text`
	font-size: 20px;
	color: rgba(25, 180, 125, 0.8);
	font-weight: 600;
	margin-bottom: 5px;
`;
const AboutTitle = styled.Text`
	font-size: 20px;
	color: rgba(0, 0, 0, 0.6);
	font-weight: 600;
	margin: 10px 0 5px;
`;
const About = styled.Text`
	font-weight: 600;
	margin-bottom: 5px;
	color: rgba(0, 0, 0, 0.4);
	line-height: 20px;
`;
const Root = styled.View`
	flex: 1;
	flex-direction: row;
	min-height: 100px;
	background: white;
	padding: 25px;
	box-shadow: 0 6px 8px rgba(0, 0, 0, 0.09);
`;

const ClinicianPhoto = styled.Image`
	width: 70px;
	height: 70px;
	border-radius: 40px;
`;

const RightSide = styled.View`
	margin: auto 20px;
`;

const Title = styled.Text`
	font-size: 18px;
	color: rgba(0, 0, 0, 0.6);
	font-weight: 600;
`;

const Specialty = styled.Text`
	font-size: 15px;
	color: rgba(0, 0, 0, 0.5);
	font-weight: 500;
`;

const Parallel = styled.View`
	flex-direction: row;
	margin-bottom: 10px;
`;
