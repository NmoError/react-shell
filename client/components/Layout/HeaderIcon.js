import { Ionicons } from '@expo/vector-icons';
import * as React from 'react';

import Colors from '../../constants/Colors';

export default function HeaderIcon({ name }) {
	return (
		<Ionicons
			name={name}
			size={30}
			style={{ marginRight: 20, marginLeft: 20 }}
			color={Colors.tabIconDefault}
		/>
	);
}
