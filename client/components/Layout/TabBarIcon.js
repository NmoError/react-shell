import { Ionicons } from '@expo/vector-icons';
import { Image } from 'react-native';
import * as React from 'react';

import Colors from '../../constants/Colors';

export default function TabBarIcon({ name, focused, secondary }) {
	let color = focused ? Colors.tabIconSelected : Colors.tabIconDefault;
	if (secondary) {
		return (
			<Image
				source={require('../../assets/icons/navigation/appointment.png')}
				style={{
					height: 27,
					resizeMode: 'contain',
					marginTop: 6,
				}}
			/>
		);
	}

	return <Ionicons name={name} size={35} color={color} />;
}
