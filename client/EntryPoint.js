import * as React from 'react';
import { Platform, StatusBar, View, Text } from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

// Navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTabNavigator from './navigation/BottomTabNavigator';
import useLinking from './navigation/useLinking';
const Stack = createStackNavigator();

// Screens
import GeneralInformation from './screens/GeneralInformation';
import MedicalTests from './screens/MedicalTests';
import MedicalRecords from './screens/MedicalRecords';
import PersonalDoctors from './screens/PersonalDoctors';
import AuthScreen from './screens/AuthScreen';

// Screen Modals
import NotificationsModal from './screens/modals/NotificationsModal';
import EditProfileModal from './screens/modals/EditProfileModal';
import InboxModal from './screens/modals/InboxModal';
import ReminderModal from './screens/modals/ReminderModal';
import SettingsModal from './screens/modals/SettingsModal';

// Redux
import { connect } from 'react-redux';

// Constants
import { headerButtonOptions, navigationTheme } from './constants/Options';

const EntryPoint = ({ skipLoadingScreen, isAuthenticated }) => {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  // Load resources prior app start
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();
        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
        });
      } catch (e) {
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }
    loadResourcesAndDataAsync();
  }, []);

  if (!isLoadingComplete && !skipLoadingScreen) {
    return null;
  }
  const MainStack = createStackNavigator();
  const RootStack = createStackNavigator();

  const AuthenticationView = () => {
    return <AuthScreen />;
  };

  function MainStackScreen({ route }) {
    return (
      <MainStack.Navigator>
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name='Root'
          component={BottomTabNavigator}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name='General Information'
          component={GeneralInformation}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name='Medical Tests'
          component={MedicalTests}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name='My Records'
          component={MedicalRecords}
        />
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name='My Doctors'
          component={PersonalDoctors}
        />
      </MainStack.Navigator>
    );
  }

  function RootStackScreen() {
    return (
      <RootStack.Navigator mode='modal'>
        <RootStack.Screen
          name='Main'
          component={MainStackScreen}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name='NotificationModal'
          component={NotificationsModal}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name='EditProfileModal'
          component={EditProfileModal}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name='InboxModal'
          component={InboxModal}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name='ReminderModal'
          component={ReminderModal}
          options={{ headerShown: false }}
        />
        <RootStack.Screen
          name='SettingsModal'
          component={SettingsModal}
          options={{ headerShown: false }}
        />
      </RootStack.Navigator>
    );
  }

  const AuthenticatedView = () => {
    return (
      <View style={{ flex: 1 }}>
        {Platform.OS === 'ios' && <StatusBar barStyle='dark-content' />}
        <NavigationContainer
          theme={navigationTheme}
          ref={containerRef}
          initialState={initialNavigationState}
        >
          <RootStackScreen />
        </NavigationContainer>
      </View>
    );
  };

  return isAuthenticated ? AuthenticatedView() : AuthenticationView();
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(EntryPoint);
