import { SET_ALERT, REMOVE_ALERT } from './types';
import { v4 as uuidv4 } from 'uuid';

export const setAlert = (msg) => (dispatch) => {
  console.log(msg);
  const id = 12324;
  dispatch({
    type: SET_ALERT,
    payload: { msg, id },
  });
};

export const removeAlert = (id) => (dispatch) => {
  dispatch({
    type: REMOVE_ALERT,
    payload: id,
  });
};
