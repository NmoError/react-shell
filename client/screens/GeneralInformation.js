import * as React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { RefreshControl, SafeAreaView, TouchableOpacity } from 'react-native';
import styled from 'styled-components';
import BodyStats from '../components/BodyStats/BodyStats';
import BloodPressureGraph from '../components/BloodPressureGraph';
import ProfileBrief from '../components/ProfileBrief';
import HeaderIcon from '../components/Layout/HeaderIcon';

function wait(timeout) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
}

export default function GeneralInformation({ navigation }) {
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  return (
    <SafeAreaView>
      <Bar>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <HeaderIcon name='md-arrow-back' />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.push('EditProfileModal')}>
          <HeaderIcon name='md-settings' />
        </TouchableOpacity>
      </Bar>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <ProfileBrief />
        <BodyStats />
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <BloodPressureGraph navigation={navigation} />
          <BloodPressureGraph navigation={navigation} />
          <BloodPressureGraph navigation={navigation} />
        </ScrollView>
        <Summary>
          <Title>General Information</Title>
          <Line></Line>
          <Info>
            <InfoType>
              Gender: <Ans>Male</Ans>
            </InfoType>
            <InfoType>
              Age: <Ans>25</Ans>
            </InfoType>
            <InfoType>
              Date of Birth: <Ans>14/02/1995</Ans>
            </InfoType>
            <InfoType>
              Address: <Ans>7 Wharf Close, Hamilton</Ans>
            </InfoType>
          </Info>
        </Summary>
      </ScrollView>
    </SafeAreaView>
  );
}

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  margin-right: 0;
  justify-content: space-between;
  flex-direction: row;
`;

const Line = styled.View`
  width: 100%;
  border: 1px dashed rgba(99, 189, 153, 1);
`;

const Summary = styled.View`
  margin: 5px;
  padding: 0 20px;
`;

const Title = styled.Text`
  font-size: 22px;
  color: rgba(99, 189, 153, 1);
  font-weight: 700;
  margin-top: 8px;
`;

const Info = styled.View`
  flex: 1;
  margin-top: 10px;
`;

const InfoType = styled.Text`
  margin: 5px 0;
  font-weight: 600;
  font-size: 18px;
  color: rgba(118, 118, 118, 1);
`;

const Ans = styled.Text`
  margin: 5px 0;
  font-weight: 500;
  font-size: 16px;
  color: rgba(118, 118, 118, 0.7);
`;
