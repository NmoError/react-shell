import * as React from 'react';
import { Animated, TouchableOpacity, Dimensions, Switch } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

import Sizes from '../../constants/Sizes';

const ReminderModal = ({ navigation }) => {
  return (
    <Root>
      <Handle />
      <Bar>
        <Title>Reminder</Title>
      </Bar>
    </Root>
  );
};

const Root = styled.SafeAreaView`
  width: 100%;
  margin: 0 auto;
  margin-top: 32px;
`;

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  font-size: ${Sizes.ScreenTitle};
`;

const Handle = styled.View`
  width: 20%;
  height: 4px;
  background: rgba(233, 233, 233, 1);
  margin: 0px auto 0;
  border-radius: 3px;
`;

const mapStateToProps = (state) => ({
  menuStatus: state.menu,
});

export default connect(mapStateToProps)(ReminderModal);
