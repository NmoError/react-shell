import * as React from 'react';
import { Animated, TouchableOpacity, Dimensions, Switch } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

import Sizes from '../../constants/Sizes';
import { ScrollView } from 'react-native-gesture-handler';

const SettingsModal = ({ navigation }) => {
  const [search, setSearch] = React.useState('');
  return (
    <Root>
      <Handle />
      <Bar>
        <SearchInput
          placeholder='Search Settings'
          name='search'
          value={search}
          onChangeText={(e) => setSearch(e)}
        />
      </Bar>
      <ScrollView>
        <Section>
          <SectionDescription>
            <Title>Account Settings</Title>
            <Subtitle>
              Manage information about you, your registered clinics and account
              in general
            </Subtitle>
          </SectionDescription>
          <List></List>
        </Section>
        <Division />
        <Section>
          <SectionDescription>
            <Title>Security</Title>
            <Subtitle>
              Change your password and take other actions to add more security
              to your account
            </Subtitle>
          </SectionDescription>
          <List></List>
        </Section>
        <Division />
        <Section>
          <SectionDescription>
            <Title>Privacy</Title>
            <Subtitle>
              Control who sees your basic or advanced health statistics
            </Subtitle>
          </SectionDescription>
          <List></List>
        </Section>
        <Division />
        <Division />
        <Section>
          <SectionDescription>
            <Title>Notifications</Title>
            <Subtitle>
              Decide how you want to communicate across ****** and how you want
              to be notified
            </Subtitle>
          </SectionDescription>
          <List></List>
        </Section>
        <Division />
      </ScrollView>
    </Root>
  );
};

const Root = styled.SafeAreaView`
  flex: 1;
  width: 100%;
  margin: 0 auto;
  margin-top: 32px;
`;

const Handle = styled.View`
  width: 20%;
  height: 4px;
  background: rgba(233, 233, 233, 1);
  margin: 0px auto 0;
  border-radius: 3px;
`;

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  font-size: 16px;
`;

const Subtitle = styled.Text`
  margin-top: 5px;
  font-size: 12px;
  color: rgba(135, 135, 135, 1);
`;

const SearchInput = styled.TextInput`
  padding: 0px 15px;
  width: 90%;
  border-radius: 7px;
  margin: 15px auto 15px auto;
  background: rgba(233, 233, 233, 1);
  font-size: 16px;
  height: 40px;
`;

const Section = styled.View`
  padding: 20px;
`;

const SectionDescription = styled.View``;

const List = styled.View`
  height: 200px;
`;

const Division = styled.View`
  height: 5px;
  background: rgba(233, 233, 233, 1);
`;

const mapStateToProps = (state) => ({
  menuStatus: state.menu,
});

export default connect(mapStateToProps)(SettingsModal);
