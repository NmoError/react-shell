import * as React from 'react';
import { Animated, TouchableOpacity, Dimensions, Switch } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Ionicons from '../../components/Tiny/Ionicons';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

import Sizes from '../../constants/Sizes';

const InboxModal = ({ navigation }) => {
  return (
    <Root>
      <Handle />
      <Bar>
        <Title>Inbox</Title>
      </Bar>
      <InboxList>
        <Mail>
          <PersonPhoto
            source={require('../../assets/images/woman_doc.jpg')}
          ></PersonPhoto>
          <PersonName>Maria Soft</PersonName>
          <Topic>Signed e-prescription attached</Topic>
          <FloatRight>
            <Time>6:23 pm</Time>
            <Ionicons
              name='md-arrow-forward'
              size={20}
              mr='auto'
              ml='auto'
              color='rgba(150, 150, 150, 1)'
            />
          </FloatRight>
        </Mail>
        <DivisionLine />
        <Mail>
          <PersonPhoto
            source={require('../../assets/images/clinic.png')}
          ></PersonPhoto>
          <PersonName>iHealth -Smart Clinic</PersonName>
          <Topic>You have a new appointment</Topic>
          <FloatRight>
            <Time>1:15 pm</Time>
            <Ionicons
              name='md-arrow-forward'
              size={20}
              mr='auto'
              ml='auto'
              color='rgba(150, 150, 150, 1)'
            />
          </FloatRight>
        </Mail>
        <DivisionLine />
        <Mail>
          <PersonPhoto
            source={require('../../assets/images/woman_doc.jpg')}
          ></PersonPhoto>
          <PersonName>Maria Soft</PersonName>
          <Topic>Hi, Mr Alejandro! I will be your personal cl...</Topic>
          <FloatRight>
            <Time>1:15 pm</Time>
            <Ionicons
              name='md-arrow-forward'
              size={20}
              mr='auto'
              ml='auto'
              color='rgba(150, 150, 150, 1)'
            />
          </FloatRight>
        </Mail>
        <DivisionLine />
        <Mail>
          <PersonPhoto
            source={require('../../assets/images/clinic.png')}
          ></PersonPhoto>
          <PersonName>iHealth -Smart Clinic</PersonName>
          <Topic>You have successfully linked with your per...</Topic>
          <FloatRight>
            <Time>1:11 pm</Time>
            <Ionicons
              name='md-arrow-forward'
              size={20}
              mr='auto'
              ml='auto'
              color='rgba(150, 150, 150, 1)'
            />
          </FloatRight>
        </Mail>
        <DivisionLine />
        <Mail>
          <PersonPhoto
            source={require('../../assets/images/clinic.png')}
          ></PersonPhoto>
          <PersonName>iHealth -Smart Clinic</PersonName>
          <Topic>Welcome to iHealth Clinic</Topic>
          <FloatRight>
            <Time>8:09 am</Time>
            <Ionicons
              name='md-arrow-forward'
              size={20}
              mr='auto'
              ml='auto'
              color='rgba(150, 150, 150, 1)'
            />
          </FloatRight>
        </Mail>
        <DivisionLine />
      </InboxList>
    </Root>
  );
};

const Root = styled.SafeAreaView`
  width: 100%;
  margin: 0 auto;
  margin-top: 32px;
`;

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  font-size: ${Sizes.ScreenTitle};
`;

const Handle = styled.View`
  width: 20%;
  height: 4px;
  background: rgba(233, 233, 233, 1);
  margin: 0px auto 0;
  border-radius: 3px;
`;

const InboxList = styled.ScrollView`
  padding: 16px;
`;

const Mail = styled.TouchableOpacity`
  position: relative;
  padding: 12px 0px 12px 65px;
  height: 60px;
`;

const DivisionLine = styled.View`
  height: 1px;
  background: rgba(200, 200, 200, 0.4);
`;

const PersonName = styled.Text``;

const Topic = styled.Text`
  color: rgba(150, 150, 150, 1);
  width: 90%;
  margin-top: 1px;
`;

const PersonPhoto = styled.Image`
  width: 50px;
  height: 50px;
  border-radius: 25px;
  position: absolute;
  top: 5px;
  transform: translateY(0px);
`;

const FloatRight = styled.View`
  position: absolute;
  right: 0;
  top: 8px;
`;

const Time = styled.Text`
  position: absolute;
  top: 2px;
  right: 20px;
  font-size: 12px;
  color: rgba(150, 150, 150, 1);
  width: 60px;
`;

const mapStateToProps = (state) => ({
  menuStatus: state.menu,
});

export default connect(mapStateToProps)(InboxModal);
