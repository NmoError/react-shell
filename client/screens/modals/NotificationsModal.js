import * as React from 'react';
import { Animated, TouchableOpacity, Dimensions, Switch } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

import Sizes from '../../constants/Sizes';

const NotificationsModal = ({ navigation }) => {
  const [enabled, setEnabled] = React.useState(false);
  return (
    <Root>
      <Handle />
      <Bar>
        <Title>Notifications</Title>
      </Bar>
      <ListItem>
        <Item>Allow Notifications</Item>
        <Switch value={enabled} onValueChange={() => setEnabled(!enabled)} />
      </ListItem>
      <DivisionLine />
      <ListItem>
        <Item>at</Item>
      </ListItem>
      <DivisionLine />
      <ListItem>
        <Item>5 minutes earlier (Video Calls)</Item>
        <Switch value={enabled} onValueChange={() => setEnabled(!enabled)} />
      </ListItem>
    </Root>
  );
};

const Root = styled.SafeAreaView`
  width: 100%;
  margin: 0 auto;
  margin-top: 32px;
`;

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  align-items: center;
  justify-content: center;
`;

const Handle = styled.View`
  width: 20%;
  height: 4px;
  background: rgba(233, 233, 233, 1);
  margin: 0px auto 0;
  border-radius: 3px;
`;

const Title = styled.Text`
  font-size: ${Sizes.ScreenTitle};
`;

const ListItem = styled.View`
  min-height: 40px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 0;
`;

const Item = styled.Text``;

const DivisionLine = styled.View`
  height: 1px;
  background: rgba(0, 0, 0, 0.1);
`;

const mapStateToProps = (state) => ({
  menuStatus: state.menu,
});

export default connect(mapStateToProps)(NotificationsModal);
