import * as React from 'react';
import {
  TouchableOpacity,
  SafeAreaView,
  View,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';
import styled from 'styled-components';
import BG from '../assets/images/leaf.png';
import LOGO from '../assets/images/logo.png';
import { connect } from 'react-redux';
import Colors from '../constants/Colors';
import { setLoading, login } from '../actions/auth';
import CliniciansL from '../assets/images/clinicians.gif';
import { StatusBar, Keyboard } from 'react-native';

// Sub Views
import Login from './LoginScreen';
import Signup from './SignupScreen';

const AuthScreen = ({ setLoading, loading }) => {
  const [landing, setLanding] = React.useState(true);
  const [toggle, setToggle] = React.useState(false);
  const [focused, setFocused] = React.useState(false);

  React.useEffect(() => {
    StatusBar.setBarStyle('dark-content');
  }, []);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss(), setFocused(false);
      }}
    >
      <SafeAreaView style={{ height: '100%' }}>
        <Cli source={CliniciansL}></Cli>
        {focused && <Blank />}
        <Logo source={LOGO} />
        <BrandText>MEDICARE</BrandText>
        {landing ? (
          <GetStarted>
            <TouchableOpacity onPress={() => setLanding(false)}>
              <GetStartedText>Get Started</GetStartedText>
            </TouchableOpacity>
          </GetStarted>
        ) : toggle ? (
          <Signup setToggle={setToggle} setFocused={setFocused} />
        ) : (
          <Login setToggle={setToggle} setFocused={setFocused} />
        )}
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

const Blank = styled.View`
  position: absolute;
  width: 100%;
  height: 100%;
  background: white;
`;

const GetStarted = styled.View`
  padding: 10px;
  background: ${Colors.defaultButton};
  width: 60%;
  border-radius: 15px;
  margin: auto auto 40px auto;
  box-shadow: 0 6px 8px rgba(0, 0, 0, 0.1);
`;

const GetStartedText = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  color: white;
`;
const BrandText = styled.Text`
  font-size: 25px;
  font-weight: 700;
  text-align: center;
`;

const Logo = styled.Image`
  margin: 55px auto 10px;
`;

const Cli = styled.Image`
  margin: 50% auto;
  resize-mode: stretch;
  width: 90%;
  height: 320px;
  position: absolute;
  align-self: center;
`;

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  loading: state.auth.loading,
});

export default connect(mapStateToProps, { setLoading, login })(AuthScreen);
