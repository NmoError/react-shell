import * as React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components';
import ScreenCard from '../components/ScreenCard';
import Profile from '../assets/images/asdf.png';
import HeaderIcon from '../components/Layout/HeaderIcon';

export default function MenuScreen({ navigation, route }) {
  React.useEffect(() => {
    console.log(route);
  }, []);

  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <ProfileImage source={Profile}></ProfileImage>
      <ScrollView
        style={{
          backgroundColor: 'white',
          borderRadius: 20,
          marginTop: 320,
        }}
      >
        <Name>Alejandro Gonzalez</Name>
        <Subtitle>Brisbane</Subtitle>
        <ScreenCard
          navigation={navigation}
          destination='General Information'
          title='General Information'
          subtitle='Basic medical information about blood type, allergies and chronic diseases'
        />
        <ScreenCard
          navigation={navigation}
          destination='My Records'
          title='Medical Records'
          subtitle='Prescriptions and recommendations from doctors in one place'
        />
        <ScreenCard
          navigation={navigation}
          destination='Medical Tests'
          title='Medical Results'
          subtitle='All your medical tests with analytics, graphics and dynamics'
        />
        <ScreenCard
          navigation={navigation}
          destination='My Doctors'
          title='My Doctors'
        />
      </ScrollView>
      <Settings>
        <TouchableOpacity onPress={() => navigation.push('SettingsModal')}>
          <HeaderIcon name='md-settings' />
        </TouchableOpacity>
      </Settings>
    </View>
  );
}

const ProfileImage = styled.Image`
  position: absolute;
  width: 100%;
  height: 400px;
`;

const Name = styled.Text`
  font-size: 24px;
  font-weight: 600;
  color: #63bd99;
  margin: 30px auto 0;
`;

const Subtitle = styled.Text`
  font-size: 18px;
  color: rgba(0, 0, 0, 0.6);
  font-weight: 500;
  margin: 10px auto 10px;
`;

const Summary = styled.View`
  flex: 1;
  margin: auto;
`;

const Title = styled.Text`
  font-size: 20px;
  color: rgba(99, 189, 153, 1);
  font-weight: 700;
`;

const Info = styled.View`
  flex: 1;
`;

const Settings = styled.View`
  position: absolute;
  margin-top: 335;
  right: 0px;
`;
