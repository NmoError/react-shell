import * as React from 'react';
import { View, SafeAreaView, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ClinicianCard from '../components/ClinicianCard';
import HeaderIcon from '../components/Layout/HeaderIcon';
import styled from 'styled-components';

export default function PersonalDoctors({ navigation }) {
  return (
    <SafeAreaView>
      <Bar>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <HeaderIcon name='md-arrow-back' />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.push('EditProfileModal')}>
          <HeaderIcon name='md-settings' />
        </TouchableOpacity>
      </Bar>
      <ScrollView style={{ paddingBottom: 20 }}>
        <ClinicianCard />
      </ScrollView>
    </SafeAreaView>
  );
}

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  margin-right: 0;
  justify-content: space-between;
  flex-direction: row;
`;
