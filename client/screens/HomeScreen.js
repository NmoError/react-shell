import * as React from 'react';
import { SafeAreaView, RefreshControl } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { logout } from '../actions/auth';
import Category from '../components/CategorySlider/Category';

function wait(timeout) {
	return new Promise((resolve) => {
		setTimeout(resolve, timeout);
	});
}

const HomeScreen = ({ logout }) => {
	const [refreshing, setRefreshing] = React.useState(false);

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);

		wait(2000).then(() => setRefreshing(false));
	}, [refreshing]);

	return (
		<SafeAreaView>
			<ScrollView
				style={{ height: '100%' }}
				refreshControl={
					<RefreshControl
						refreshing={refreshing}
						onRefresh={onRefresh}
					/>
				}
			>
				<Title>Categories</Title>
				<ScrollView
					horizontal={true}
					showsHorizontalScrollIndicator={false}
					style={{ padding: 5 }}
				>
					{data.map((a) => (
						<Category
							key={a.title}
							title={a.title}
							color={a.color}
						/>
					))}
				</ScrollView>
			</ScrollView>
		</SafeAreaView>
	);
};

const data = [
	{
		title: 'Urgent Care',
		color: 'red',
	},
	{
		title: 'Dental',
		color: 'rgba(64,190,250,1)',
	},
	{
		title: 'Mental Health',
		color: 'rgba(130,98,248,1)',
	},
	{
		title: 'Physio',
		color: 'rgba(255,166, 63,1)',
	},
	{
		title: 'Cardiology',
		color: 'rgba(255,60,60,1)',
	},

	{
		title: 'Orthopedic',
		color: 'rgba(255, 160, 114, 1)',
	},
	{
		title: 'Radiology',
		color: 'rgba(118,118,118,1)',
	},
	{
		title: 'Skin Care',
		color: 'rgba(255, 179, 172, 1);',
	},
];

HomeScreen.navigationOptions = {
	header: null,
};

const Title = styled.Text`
	font-size: 24px;
	font-weight: 600;
	color: rgba(100, 100, 100, 1);
	padding: 20px 20px 10px;
`;

export default connect(null, { logout })(HomeScreen);
