import * as React from 'react';
import { Text, View } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Colors from '../constants/Colors';
import { login } from '../actions/auth';

const SignupScreen = ({ login, loading, setToggle, setFocused }) => {
	const [name, setName] = React.useState('');
	const [email, setEmail] = React.useState('');

	return (
		<SignInForm behavior='padding'>
			<View style={{ margin: 'auto' }}>
				<Input
					placeholder='Name '
					value={name}
					onChangeText={(e) => setName(e)}
					onFocus={() => setFocused(true)}
				/>
				<Input
					placeholder='Email'
					value={email}
					onFocus={() => {
						setFocused(true);
					}}
					onChangeText={(e) => setEmail(e)}
				/>
			</View>
			<GetStarted disabled={loading}>
				<GetStartedText>Continue</GetStartedText>
			</GetStarted>
			<Toggle onPress={() => setToggle(false)}>
				<Text>I already have an account</Text>
			</Toggle>
		</SignInForm>
	);
};

const Toggle = styled.TouchableOpacity`
	align-self: center;
	align-items: center;
	border-radius: 7px;
	border: 1px solid ${Colors.defaultButton};
	padding: 10px;
	width: 60%;
	margin-bottom: 40px;
`;

const Input = styled.TextInput`
	padding: 15px;
	width: 60%;
	border-radius: 7px;
	margin: auto auto 15px auto;
	background: rgba(255, 255, 255, 1);
	font-size: 16px;
	box-shadow: 0 6px 8px rgba(0, 0, 0, 0.1);
`;

const SignInForm = styled.KeyboardAvoidingView`
	margin: auto 0 0 0;
	background: transparent;
`;

const GetStarted = styled.TouchableOpacity`
	padding: 10px;
	background: ${Colors.defaultButton};
	width: 60%;
	border-radius: 7px;
	border: none;
	margin: 5px auto 20px auto;
	box-shadow: 0 6px 8px rgba(0, 0, 0, 0.1);
`;

const GetStartedText = styled.Text`
	font-size: 20px;
	font-weight: 600;
	text-align: center;
	color: white;
`;

const mapStateToProps = (state) => ({
	isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(SignupScreen);
