import * as React from 'react';
import { Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components';
import ProfileBrief from '../components/ProfileBrief';
import MedicalTest from '../components/MedicalTest/MedicalTest';
import HeaderIcon from '../components/Layout/HeaderIcon';

export default function MedicalTests({ navigation }) {
  return (
    <SafeAreaView>
      <Bar>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <HeaderIcon name='md-arrow-back' />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.push('EditProfileModal')}>
          <HeaderIcon name='md-settings' />
        </TouchableOpacity>
      </Bar>
      <ProfileBrief />
      <ScrollView horizontal={true} pagingEnabled={true}>
        <MedicalTest />
        <MedicalTest />
        <MedicalTest />
        <MedicalTest />
      </ScrollView>
    </SafeAreaView>
  );
}

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  margin-right: 0;
  justify-content: space-between;
  flex-direction: row;
`;

const ww = Dimensions.get('window').width;

const Root = styled.View``;
