import * as React from 'react';
import { TouchableOpacity, Text, StatusBar } from 'react-native';
import styled from 'styled-components';
import { connect } from 'react-redux';
import Colors from '../constants/Colors';
import { login } from '../actions/auth';

const LoginScreen = ({ login, loading, setToggle, setFocused }) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  return (
    <SignInForm behavior='padding'>
      <Input
        name='email'
        placeholder='Email'
        value={email}
        onChangeText={(e) => setEmail(e)}
        onFocus={() => {
          setFocused(true);
        }}
      ></Input>
      <Input
        name='password'
        placeholder='Password'
        value={password}
        secureTextEntry={true}
        onFocus={() => {
          setFocused(true);
        }}
        onChangeText={(e) => setPassword(e)}
      ></Input>
      <GetStarted
        style={{ marginTop: 5 }}
        disabled={loading}
        onPress={() => login(email, password)}
      >
        <GetStartedText>Log in</GetStartedText>
      </GetStarted>
      <Toggle onPress={() => setToggle(true)}>
        <Text>I don't have an account</Text>
      </Toggle>
    </SignInForm>
  );
};

const Toggle = styled.TouchableOpacity`
  align-self: center;
  align-items: center;
  border-radius: 7px;
  border: 1px solid ${Colors.defaultButton};
  padding: 10px;
  width: 60%;
  margin-bottom: 40px;
`;

const Input = styled.TextInput`
  padding: 15px;
  width: 60%;
  border-radius: 7px;
  margin: auto auto 15px auto;
  background: rgba(255, 255, 255, 1);
  font-size: 16px;
  box-shadow: 0 6px 8px rgba(0, 0, 0, 0.1);
`;

const SignInForm = styled.KeyboardAvoidingView`
  margin: auto 0 0 0;
`;

const GetStarted = styled.TouchableOpacity`
  padding: 10px;
  background: ${(props) => (props.disabled ? `#eee` : Colors.defaultButton)};
  width: 60%;
  border-radius: 7px;
  border: none;
  margin: auto auto 20px auto;
  box-shadow: 0 6px 8px rgba(0, 0, 0, 0.1);
`;

const GetStartedText = styled.Text`
  font-size: 20px;
  font-weight: 600;
  text-align: center;
  color: white;
`;

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { login })(LoginScreen);
