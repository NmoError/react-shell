import * as React from 'react';
import { SafeAreaView, RefreshControl } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import styled from 'styled-components';
import DaySummary from '../components/DaySummary/DaySummary';
import { connect } from 'react-redux';

import { setAlert } from '../actions/alert';

import { CalendarList } from 'react-native-calendars';
import HeaderIcon from '../components/Layout/HeaderIcon';

function wait(timeout) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
}

const CalendarScreen = ({ route, navigation, setAlert }) => {
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);

  const [day, setDay] = React.useState();
  React.useEffect(() => {
    console.log(route);
  }, []);

  return (
    <SafeAreaView>
      <Bar>
        <TouchableOpacity onPress={() => navigation.push('InboxModal')}>
          <HeaderIcon name='md-mail-open' />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setAlert()}>
          <HeaderIcon name='md-add' />
        </TouchableOpacity>
      </Bar>
      <ScrollView
        style={{ height: '100%' }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <Root>
          <CalendarList
            horizontal={true}
            pagingEnabled={true}
            hideArrows={false}
            onDayPress={(d) => {
              setDay(d);
            }}
            markedDates={{
              '2020-05-20': {
                marked: true,
              },
            }}
            theme={theme}
          />
          {day && day.day == 20 ? (
            <DaySummary event={day} navigation={navigation} />
          ) : (
            <EmptySummary>
              <Title>No Appointments for this day.</Title>
            </EmptySummary>
          )}
        </Root>
      </ScrollView>
    </SafeAreaView>
  );
};

CalendarScreen.navigationOptions = {
  headerTransparent: true,
};

const theme = {
  backgroundColor: '#ffffff',
  calendarBackground: '#ffffff',
  textSectionTitleColor: 'rgba(0, 0, 0, 0.6)',
  selectedDayBackgroundColor: '#ff9653',
  selectedDayTextColor: '#ffffff',
  todayTextColor: '#ff9653',
  dayTextColor: '#2d4150',
  textDisabledColor: '#d9e1e8',
  dotColor: '#ff9653',
  selectedDotColor: '#ffffff',
  arrowColor: '#ff9653',
  disabledArrowColor: '#d9e1e8',
  textDayFontWeight: '300',
  textMonthFontWeight: 'bold',
  textDayHeaderFontWeight: '300',
  textDayFontSize: 16,
  textMonthFontSize: 16,
  textDayHeaderFontSize: 16,
};

const Root = styled.View``;

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  margin-right: 0;
  justify-content: space-between;
  flex-direction: row;
`;

const EmptySummary = styled.View`
  margin: 20px;
  border-radius: 14px;
  box-shadow: 0 -8px 7px rgba(0, 0, 0, 0.05);
  background: white;
  padding: 25px;
`;

const Title = styled.Text`
  text-align: center;
  font-size: 19px;
  font-weight: 500;
  color: rgba(0, 0, 0, 0.6);
`;

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { setAlert })(CalendarScreen);
