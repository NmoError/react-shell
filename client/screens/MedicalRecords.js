import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components';
import BodyStats from '../components/BodyStats/BodyStats';
import BloodPressureGraph from '../components/BloodPressureGraph';
import ProfileBrief from '../components/ProfileBrief';
import Record from '../components/Record';
import HeaderIcon from '../components/Layout/HeaderIcon';

export default function MedicalRecords({ navigation }) {
  return (
    <SafeAreaView>
      <Bar>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <HeaderIcon name='md-arrow-back' />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.push('EditProfileModal')}>
          <HeaderIcon name='md-settings' />
        </TouchableOpacity>
      </Bar>
      <ProfileBrief />
      <ScrollView>
        <SectionTitle>Recent Records</SectionTitle>
        <Record />
        <Record />
        <Record />
      </ScrollView>
    </SafeAreaView>
  );
}

const Bar = styled.View`
  height: 50px;
  background: transparent;
  padding: 10px 0;
  margin-right: 0;
  justify-content: space-between;
  flex-direction: row;
`;

const SectionTitle = styled.Text`
  font-size: 26px;
  font-weight: 600;
  color: rgba(0, 0, 0, 0.6);
  padding: 0 35px;
  margin-bottom: 10px;
`;
