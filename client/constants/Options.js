import HeaderIcon from '../components/Layout/HeaderIcon';
import * as React from 'react';
import { TouchableOpacity } from 'react-native';

export const headerButtonOptions = {
  headerRight: () => (
    <TouchableOpacity>
      <HeaderIcon name='md-settings' />
    </TouchableOpacity>
  ),
  headerBackImage: () => <HeaderIcon name='md-arrow-back' />,
  headerTintColor: 'rgba(0,0,0,0.7)',
  headerTitleStyle: {
    fontSize: 20,
  },
};

export const navigationTheme = {
  colors: {
    primary: '#ff9653',
    background: 'rgb(255, 253, 253)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'white',
  },
};
