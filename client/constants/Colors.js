const tintColor = '#ff9653';
const defaultButton = 'rgba(72,193,156,1)';

export default {
	tintColor,
	defaultButton,
	defaultSofter: 'rgba(72,193,156,0.2)',
	defaultSoftest: 'rgba(72,193,156,0.05)',
	modalBackground: 'rgba(0,0,0,0.6)',
	secondaryFont: 'rgba(0,0,0,0.6)',
	tabIconDefault: 'rgba(72,193,156,1)',
	tabIconSelected: tintColor,
	tabBar: '#fefefe',
	errorBackground: 'red',
	errorText: '#fff',
	warningBackground: '#EAEB5E',
	warningText: '#666804',
	noticeBackground: tintColor,
	noticeText: '#fff',
};
