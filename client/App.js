import * as React from 'react';

// Redux
import { Provider } from 'react-redux';
import store from './store';
import EntryPoint from './EntryPoint';

import Alert from './components/Layout/Alert';

export default function App() {
	return (
		<Provider store={store}>
			<EntryPoint />
			<Alert />
		</Provider>
	);
}
