import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';

import TabBarIcon from '../components/Layout/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import MenuScreen from '../screens/MenuScreen';
import CalendarScreen from '../screens/CalendarScreen';
import MapScreen from '../screens/MapScreen';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

export default function BottomTabNavigator({ navigation, route }) {
	navigation.setOptions({ headerTitle: getHeaderTitle(route) });

	return (
		<BottomTab.Navigator
			initialRouteName={INITIAL_ROUTE_NAME}
			tabBarOptions={{ style: { height: 80 } }}
		>
			<BottomTab.Screen
				name='Home'
				component={HomeScreen}
				options={{
					title: ' ',
					tabBarIcon: ({ focused }) => (
						<TabBarIcon focused={focused} name='md-home' />
					),
				}}
			/>
			<BottomTab.Screen
				name='Map'
				component={MapScreen}
				options={{
					title: ' ',
					tabBarIcon: ({ focused }) => (
						<TabBarIcon focused={focused} name='ios-call' />
					),
				}}
			/>
			<BottomTab.Screen
				name='Calendar'
				component={CalendarScreen}
				options={{
					title: ' ',
					tabBarIcon: ({ focused }) => (
						<TabBarIcon focused={focused} name='md-calendar' />
					),
				}}
			/>
			<BottomTab.Screen
				name='Menu'
				component={MenuScreen}
				options={{
					title: ' ',
					tabBarIcon: ({ focused }) => (
						<TabBarIcon focused={focused} name='md-menu' />
					),
				}}
			/>
		</BottomTab.Navigator>
	);
}

function getHeaderTitle(route) {
	const routeName =
		route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

	switch (routeName) {
		case 'Home':
			return ' ';
		case 'Map':
			return ' ';
		case 'Calendar':
			return ' ';
		case 'Menu':
			return ' ';
	}
}
