const mongoose = require('mongoose');

const PatientProfileSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: true,
  },
  sex: {
    type: String,
    enum: ['male', 'female', 'non-binary'],
  },
  dob: {
    type: Date,
  },
  status: {
    type: String,
  },
  bio: {
    type: String,
  },
  mobile: [
    {
      number: {
        type: String,
        maxlength: [20, 'Phone number can not be longer than 20 characters'],
      },
      label: {
        type: String,
        enum: ['mobile', 'work', 'home', 'school', 'other'],
      },
    },
  ],
  address: [
    {
      type: String,
    },
  ],
  location: {
    // GeoJSON Point
    type: {
      type: String,
      enum: ['Point'],
    },
    coordinates: {
      type: [Number],
      index: '2dsphere',
    },
    formattedAddress: String,
    street: String,
    city: String,
    state: String,
    zipcode: String,
    country: String,
  },
  photo: {
    type: String,
    default: 'no-photo.jpg',
  },
  organizations: [
    {
      name: {
        type: String,
        required: true,
      },
      address: {
        type: String,
        required: [true, 'Please add an address'],
      },
      location: {
        // GeoJSON Point
        type: {
          type: String,
          enum: ['Point'],
        },
        coordinates: {
          type: [Number],
          index: '2dsphere',
        },
        formattedAddress: String,
        street: String,
        city: String,
        state: String,
        zipcode: String,
        country: String,
      },
    },
  ],
  clinicians: [
    {
      clinician: {
        type: mongoose.Schema.ObjectId,
        ref: 'Clinician',
        required: false,
      },
    },
  ],
  website: {
    type: String,
  },
  lastActive: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('PatientProfile', PatientProfileSchema);
